package util

import (
	"fyne.io/fyne/v2"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/assets"
)

func NewStaticResourceIcon(iconName assets.Icon) (fyne.Resource, error) {
	data, err := assets.ReadIcon(iconName)
	if err != nil {
		return nil, err
	}
	appIcon := fyne.NewStaticResource(string(iconName), data)
	return appIcon, nil
}
