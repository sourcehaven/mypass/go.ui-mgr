package ui

import (
	"github.com/sirupsen/logrus"
)

func (a *App) LogLifecycle() {
	a.App.Lifecycle().SetOnStarted(func() {
		logrus.Debug("Lifecycle: Started")
	})
	a.App.Lifecycle().SetOnStopped(func() {
		logrus.Debug("Lifecycle: Stopped")
	})
	a.App.Lifecycle().SetOnEnteredForeground(func() {
		logrus.Debug("Lifecycle: Entered Foreground")
	})
	a.App.Lifecycle().SetOnExitedForeground(func() {
		logrus.Debug("Lifecycle: Exited Foreground")
	})
}
