package ui

import "fyne.io/fyne/v2"

func (a *App) AddMainMenu() {
	a.Window.SetMainMenu(fyne.NewMainMenu(fyne.NewMenu("File"), fyne.NewMenu("View")))
}
