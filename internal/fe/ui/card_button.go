package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type CardButton struct {
	widget.Card

	OnTapped func()
}

func (b *CardButton) Tapped(*fyne.PointEvent) {
	if b.OnTapped != nil {
		b.OnTapped()
	}
}

func NewCardButton(title string, subtitle string, content fyne.CanvasObject, onTapped func()) *CardButton {
	cb := &CardButton{
		Card: widget.Card{
			Title:    title,
			Subtitle: subtitle,
			Content:  content,
		},

		OnTapped: onTapped,
	}
	//cb.ExtendBaseWidget(cb)
	return cb
}
