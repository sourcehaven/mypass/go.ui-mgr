package ui

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/assets"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/db"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/fe/util"
)

type AppConfig struct {
	Title    string
	Repo     *db.Repository
	UiConfig *Config
}

type App struct {
	App        fyne.App
	Window     fyne.Window
	NavSplit   *container.Split
	Repository *db.Repository
	Config     *Config
}

func NewApp(c ...*AppConfig) *App {
	appConf := &AppConfig{}
	if len(c) > 0 {
		appConf = c[0]
	}
	a := app.NewWithID("io.sourcehaven.mypass")
	w := a.NewWindow(appConf.Title)

	return &App{a, w, nil, appConf.Repo, appConf.UiConfig}
}

func (a *App) SetAppIcon() {
	appIcon, err := util.NewStaticResourceIcon(assets.LogoIcon)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"topic":  "icon loading",
			"reason": fmt.Sprintf("%v", err),
		}).Warn("UI failure.")
	} else {
		a.App.SetIcon(appIcon)
		a.Window.SetIcon(appIcon)
	}
}
