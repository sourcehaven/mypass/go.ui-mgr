package ui

import (
	"fyne.io/fyne/v2"
)

func (a *App) ManageConfig() error {
	conf := a.Config

	err := conf.ReadOrCreateWithDefaultValues()
	if err != nil {
		return err
	}

	isMobile := a.App.Driver().Device().IsMobile()
	if !isMobile {
		// Resize the window
		a.Window.Resize(fyne.Size{
			Width:  conf.GetWidth(),
			Height: conf.GetHeight(),
		})
		a.NavSplit.SetOffset(conf.GetNavSplitOffset())
	}

	// Function to save the configuration on close
	saveConfigFunc := func() {
		if !isMobile {
			// Get the current window size
			currentSize := a.Window.Canvas().Size()

			conf.Width = currentSize.Width
			conf.Height = currentSize.Height
			conf.NavSplitOffset = a.NavSplit.Offset
			err = conf.Write()
		}
	}

	// Register the saveConfigFunc function to be called on application close
	a.App.Lifecycle().SetOnStopped(saveConfigFunc)
	return nil
}
