package ui

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/cmd/fyne_demo/tutorials"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"
)

var (
	placeholderIcon  fyne.Resource
	vaultIcon        fyne.Resource
	noteIcon         fyne.Resource
	favouriteIconYes fyne.Resource
	favouriteIconNo  fyne.Resource
	editIcon         fyne.Resource
	deleteIcon       fyne.Resource
)

func Init() {
	placeholderIcon = theme.BrokenImageIcon()
	vaultIcon = theme.DeleteIcon()
	noteIcon = theme.FileIcon()

	favouriteIconYes = theme.StorageIcon()
	favouriteIconNo = theme.DeleteIcon()
	editIcon = theme.DocumentCreateIcon()
	deleteIcon = theme.DeleteIcon()
}

// AddContent creates the content for the main user mid,
// and is responsible for handling different UI components.
func (a *App) AddContent() {
	folders, err := a.Repository.GetAllFolder()
	if err != nil {
		panic(err)
	}

	split := container.NewHSplit(a.addNav(folders), a.addContent())
	split.Offset = a.Config.GetNavSplitOffset()
	a.NavSplit = split
	a.Window.SetContent(split)
}

func unsupportedTutorial(t tutorials.Tutorial) bool {
	return !t.SupportWeb && fyne.CurrentDevice().IsBrowser()
}

var navItems = map[string][]string{
	"folders": {},
	"entries": {},
}

func (a *App) addNav(folders []*model.Folder) fyne.CanvasObject {
	for _, folder := range folders {
		navItems["folders"] = append(navItems["folders"], folder.GetName())
	}

	tree := &widget.Tree{
		ChildUIDs: func(uid string) []string {
			return navItems[uid]
		},
		IsBranch: func(uid string) bool {
			children, ok := navItems[uid]

			return ok && len(children) > 0
		},
		CreateNode: func(branch bool) fyne.CanvasObject {
			return widget.NewLabel("Collection Widgets")
		},
		UpdateNode: func(uid string, branch bool, obj fyne.CanvasObject) {
			_, ok := navItems[uid]
			if !ok {
				fyne.LogError("Missing tutorial panel: "+uid, nil)
				return
			}
			/*obj.(*widget.Label).SetText(t.Title)
			if unsupportedTutorial(t) {
				obj.(*widget.Label).TextStyle = fyne.TextStyle{Italic: true}
			} else {
				obj.(*widget.Label).TextStyle = fyne.TextStyle{}
			}*/
		},
	}

	return tree
}

// Function to remove an entry from the slice
func removeEntry(entries []model.Entry, id widget.ListItemID) []model.Entry {
	index := int(id)
	// Ensure the index is within the bounds of the slice
	if index >= 0 && index < len(entries) {
		// Remove the entry at the specified index
		return append(entries[:index], entries[index+1:]...)
	}
	return entries
}

func (a *App) addContent() fyne.CanvasObject {
	Init()
	entries, err := a.Repository.GetAllEntry()

	if err != nil {
		a.ShowErrorDialog(err)
		return widget.NewLabel("Error happened: " + err.Error())
	}

	var list *widget.List
	list = widget.NewList(
		func() int {
			return len(entries)
		},
		func() fyne.CanvasObject {
			left := container.NewHBox(widget.NewIcon(placeholderIcon), widget.NewLabel("Template Object"))
			right := container.NewHBox(
				widget.NewButtonWithIcon("", favouriteIconNo, func() {}),
				widget.NewButtonWithIcon("", editIcon, func() {}),
				widget.NewButtonWithIcon("", deleteIcon, func() {}),
			)
			return container.NewBorder(nil, nil, left, right)
		},
		func(id widget.ListItemID, item fyne.CanvasObject) {
			leftCont := item.(*fyne.Container).Objects[0].(*fyne.Container)
			rightCont := item.(*fyne.Container).Objects[1].(*fyne.Container)
			favButton := rightCont.Objects[0].(*widget.Button)
			editButton := rightCont.Objects[1].(*widget.Button)
			deleteButton := rightCont.Objects[2].(*widget.Button)
			label := leftCont.Objects[1].(*widget.Label)

			entry := entries[id]

			favButton.OnTapped = func() {
				entry.ToggleFavourite()
				isFav := entry.GetFavourite()
				if isFav {
					favButton.SetIcon(favouriteIconYes)
					logrus.Debugf("item %d set to favourite %t", id, isFav)
				} else {
					favButton.SetIcon(favouriteIconNo)
					logrus.Debugf("item %d set to favourite %t", id, isFav)
				}
			}
			deleteButton.OnTapped = func() {
				a.ShowConfirmDialog(
					"Delete operation",
					fmt.Sprintf("Are you sure to delete: %s?", entry.GetName()),
					func(b bool) {
						if b {
							entries = removeEntry(entries, id)
							list.Refresh()
						}
					},
				)
			}

			switch e := entry.(type) {
			case *model.Vault:
				leftCont.Objects[0] = widget.NewIcon(vaultIcon)
				label.SetText(e.GetName() + "\n" + e.GetUsername())

				editButton.OnTapped = func() {
					a.createShowVaultEditorWindow(e)
				}

				list.SetItemHeight(id, 50)
			case *model.Note:
				leftCont.Objects[0] = widget.NewIcon(noteIcon)
				label.SetText(e.GetName())

				editButton.OnTapped = func() {
					a.createShowNoteEditorWindow(e)
				}
			}
		},
	)

	return list
}
