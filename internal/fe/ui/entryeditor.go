package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"
)

func (a *App) createEntry(inputText string, inputPlaceholder string, password bool) *widget.Entry {
	inputWidget := widget.NewEntry()
	inputWidget.Password = password
	inputWidget.SetText(inputText)
	inputWidget.SetPlaceHolder(inputPlaceholder)
	return inputWidget
}

func (a *App) createMultiLineEntry(inputText string, inputPlaceholder string) *widget.Entry {
	inputWidget := widget.NewMultiLineEntry()
	inputWidget.SetText(inputText)
	inputWidget.SetPlaceHolder(inputPlaceholder)
	return inputWidget
}

func (a *App) createLabeledEntry(label string, inputWidget *widget.Entry) *fyne.Container {
	labelWidget := widget.NewLabel(label)
	return container.NewVBox(
		widget.NewCard("", "", container.NewVBox(
			labelWidget,
			inputWidget,
		)),
	)
}

func (a *App) createShowVaultEditorWindow(entry *model.Vault) {
	win := a.App.NewWindow("Edit")

	nameEntry := a.createEntry(entry.GetName(), "", false)
	nameContainer := a.createLabeledEntry("Name", nameEntry)

	urlEntry := a.createEntry(entry.GetUrl(), "", false)
	urlContainer := a.createLabeledEntry("Url", urlEntry)

	usernameEntry := a.createEntry(entry.GetUsername(), "", false)
	usernameContainer := a.createLabeledEntry("Username", usernameEntry)

	passwordEntry := a.createEntry(entry.GetPassword(), "", true)
	passwordContainer := a.createLabeledEntry("Password", passwordEntry)

	folder := entry.GetFolder()
	folderName := ""
	if folder != nil {
		folderName = folder.GetName()
	}
	folderEntry := a.createEntry(folderName, "", false)
	folderContainer := a.createLabeledEntry("Folder", folderEntry)

	noteEntry := a.createMultiLineEntry(entry.GetNote(), "")
	noteContainer := a.createLabeledEntry("Note", noteEntry)

	labeledInputs := []*fyne.Container{
		nameContainer,
		urlContainer,
		usernameContainer,
		passwordContainer,
		folderContainer,
		noteContainer,
	}

	inputContainer := container.NewVBox(containersToCanvasObjects(labeledInputs)...)

	cancelButton := widget.NewButton("Cancel", func() {
		win.Close()
	})

	saveButton := widget.NewButton("Save", func() {
		entry.SetName(nameEntry.Text)
		//entry.SetFolder(folderEntry.Text)
		entry.SetNote(noteEntry.Text)
		//entry.SetFavourite(favouriteEntry.value)
		entry.SetPassword(passwordEntry.Text)
		entry.SetUsername(usernameEntry.Text)
		entry.SetUrl(urlEntry.Text)
		win.Close()
	})

	buttonContainer := container.NewVBox(saveButton, cancelButton)

	finalContainer := container.NewVBox(inputContainer, buttonContainer)

	win.Resize(fyne.Size{Width: DefaultWidth, Height: DefaultHeight})
	win.SetContent(finalContainer)
	win.Show()
}

func (a *App) createShowNoteEditorWindow(entry *model.Note) {
	win := a.App.NewWindow("Edit")

	nameEntry := a.createEntry(entry.GetName(), "", false)
	nameContainer := a.createLabeledEntry("Name", nameEntry)

	folderEntry := a.createEntry(entry.GetFolder().GetName(), "", false)
	folderContainer := a.createLabeledEntry("Folder", folderEntry)

	noteEntry := a.createMultiLineEntry(entry.GetNote(), "")
	noteContainer := a.createLabeledEntry("Note", noteEntry)

	labeledInputs := []*fyne.Container{
		nameContainer,
		folderContainer,
		noteContainer,
	}

	inputContainer := container.NewVBox(containersToCanvasObjects(labeledInputs)...)

	cancelButton := widget.NewButton("Cancel", func() {
		win.Close()
	})

	saveButton := widget.NewButton("Save", func() {
		entry.SetName(nameEntry.Text)
		//entry.SetFolder(folderEntry.Text)
		entry.SetNote(noteEntry.Text)
		//entry.SetFavourite(favouriteEntry.value)
		win.Close()
	})

	buttonContainer := container.NewVBox(saveButton, cancelButton)

	finalContainer := container.NewVBox(inputContainer, buttonContainer)

	win.Resize(fyne.Size{Width: DefaultWidth, Height: DefaultHeight})
	win.SetContent(finalContainer)
	win.Show()
}
