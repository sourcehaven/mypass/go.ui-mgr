package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"github.com/sirupsen/logrus"
)

func (a *App) AddSystemTray() {
	if desk, ok := a.App.(desktop.App); ok {
		menu := fyne.NewMenu("MyPass",
			fyne.NewMenuItem("Show", func() {
				a.Window.Show()
			}),
		)
		desk.SetSystemTrayMenu(menu)

		if ico := a.App.Icon(); ico != nil {
			desk.SetSystemTrayIcon(ico)
		} else {
			logrus.Warn("Unable to set system tray icon as app icon is not set")
		}

		a.Window.SetCloseIntercept(func() {
			a.Window.Hide()
		})
	} else {
		logrus.WithFields(logrus.Fields{
			"topic":  "system.tray",
			"reason": "...",
		}).Warn("UI failure.")
	}
}
