package ui

import "fyne.io/fyne/v2"

func containersToCanvasObjects(containers []*fyne.Container) []fyne.CanvasObject {
	var canvasObjects []fyne.CanvasObject
	for _, c := range containers {
		canvasObjects = append(canvasObjects, c)
	}
	return canvasObjects
}
