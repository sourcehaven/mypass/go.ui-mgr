package ui

import (
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"
	"gorm.io/gorm"
)

func (a *App) AddSampleEntries() {
	socialFolder := &model.Folder{Model: gorm.Model{ID: 1}, Name: "Social"}
	personalFolder := &model.Folder{Model: gorm.Model{ID: 2}, Name: "Personal"}
	developerFolder := &model.Folder{Model: gorm.Model{ID: 3}, Name: "Developer"}

	vault1 := &model.Vault{Model: gorm.Model{ID: 1}, Username: "Username1", Name: "facebook", Folder: socialFolder, Url: "facebook.com"}
	vault2 := &model.Vault{Model: gorm.Model{ID: 2}, Username: "Username2", Name: "youtube", Folder: personalFolder, Url: "https://youtube.com"}
	vault3 := &model.Vault{Model: gorm.Model{ID: 3}, Username: "Username3", Name: "linkedin", Folder: socialFolder, Url: "www.linkedin.com"}
	vault4 := &model.Vault{Model: gorm.Model{ID: 4}, Username: "Username4", Name: "golang", Folder: developerFolder, Url: "golang.org"}
	vault5 := &model.Vault{Model: gorm.Model{ID: 5}, Username: "Username5", Name: "github", Folder: developerFolder, Url: "github.com"}
	vault6 := &model.Vault{Model: gorm.Model{ID: 6}, Username: "Username6", Name: "google.com", Url: "http://google.com/"}
	vault7 := &model.Vault{Model: gorm.Model{ID: 7}, Username: "Username7", Name: "gmail", Url: "gmail.com"}

	err := a.Repository.CreateUpdateVault(vault1)
	err = a.Repository.CreateUpdateVault(vault2)
	err = a.Repository.CreateUpdateVault(vault3)
	err = a.Repository.CreateUpdateVault(vault4)
	err = a.Repository.CreateUpdateVault(vault5)
	err = a.Repository.CreateUpdateVault(vault6)
	err = a.Repository.CreateUpdateVault(vault7)

	if err != nil {
		panic(err.Error())
	}

	note1 := &model.Note{Model: gorm.Model{ID: 1}, Name: "Hello there", Folder: personalFolder}
	err = a.Repository.CreateUpdateNote(note1)

	if err != nil {
		panic(err.Error())
	}
}
