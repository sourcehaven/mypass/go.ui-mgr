package ui

import (
	"fyne.io/fyne/v2/dialog"
)

func (a *App) ShowErrorDialog(err error) {
	dialog.ShowError(err, a.Window)
}

func (a *App) ShowConfirmDialog(title string, message string, callback func(bool)) {
	dlg := dialog.NewConfirm(title, message, callback, a.Window)
	dlg.Show()
}
