package ui

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"os"
)

const (
	DefaultWidth          = 800
	DefaultHeight         = 600
	DefaultNavSplitOffset = 0.2
)

type Config struct {
	path           string
	Width          float32 `yaml:"width"`
	Height         float32 `yaml:"height"`
	NavSplitOffset float64 `yaml:"navSplitOffset"`
}

func (c *Config) GetWidth() float32 {
	if c.Width == 0 {
		return DefaultWidth
	}
	return c.Width
}

func (c *Config) GetHeight() float32 {
	if c.Height == 0 {
		return DefaultHeight
	}
	return c.Height
}

func (c *Config) GetNavSplitOffset() float64 {
	if c.NavSplitOffset == 0 {
		return DefaultNavSplitOffset
	}
	return c.NavSplitOffset
}

func NewConfig(path string) *Config {
	conf := &Config{path: path}
	return conf
}

func (c *Config) WriteDefault() error {
	c.Width = DefaultWidth
	c.Height = DefaultHeight
	c.NavSplitOffset = DefaultNavSplitOffset

	if err := c.Write(); err != nil {
		logrus.Error(err)
		return err
	}

	logrus.Infof("Config file %s written with default values", c.path)
	return nil
}

func (c *Config) ReadOrCreateWithDefaultValues() error {

	// Check if the file exists
	if _, err := os.Stat(c.path); os.IsNotExist(err) {
		logrus.Infof("Config file %s doesn't exist yet", c.path)

		err = c.WriteDefault()
		if err != nil {
			return err
		}
	}

	// Read YAML file
	data, err := os.ReadFile(c.path)
	if err != nil {
		logrus.Error(err)
		return err
	}

	// Unmarshal YAML data into config struct
	err = yaml.Unmarshal(data, c)
	if err != nil {
		logrus.Error(err)
		return err
	}

	logrus.Infof("Config file %s read successfully", c.path)
	return nil
}

func (c *Config) Write() error {
	// Marshal config struct to YAML data
	data, err := yaml.Marshal(c)
	if err != nil {
		logrus.Error(err)
		return err
	}

	// Write YAML data to file
	err = os.WriteFile(c.path, data, 0644)
	if err != nil {
		logrus.Error(err)
		return err
	}

	logrus.Infof("Config file %s written", c.path)
	return nil
}
