package ui

import (
	log "github.com/sirupsen/logrus"
	"net/url"
	"strings"
)

func (a *App) OpenUrl(urlName string) error {

	ensureScheme := func() string {
		for _, scheme := range [3]string{"https://", "http://", "file://"} {
			if strings.HasPrefix(urlName, scheme) {
				return urlName
			}
		}
		return "http://" + urlName
	}

	openUrlFunc := func() error {
		urlWithScheme := ensureScheme()
		u, err := url.ParseRequestURI(urlWithScheme)
		if err != nil {
			return err
		}
		return a.App.OpenURL(u)
	}

	loggingFunc := func(f func() error) error {
		err := f()
		if err != nil {
			log.Errorf("Unable to open URL with default browser: '%s'\n Error message: '%v'", urlName, err)
		} else {
			log.Infof("Opening URL with default browser: '%s'", urlName)
		}
		return err
	}

	return loggingFunc(openUrlFunc)
}
