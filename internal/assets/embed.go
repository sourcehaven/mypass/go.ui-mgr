package assets

import (
	"embed"
)

//go:embed ico
var icons embed.FS

type Icon string

// Icon paths
const (
	DeleteIcon Icon = "delete.png"
	EditIcon   Icon = "edit.jpg"
	LaunchIcon Icon = "launch.png"
	LockIcon   Icon = "lock.jpg"
	LogoIcon   Icon = "mypass.png"
	NoteIcon   Icon = "note.png"
)

func ReadIcon(name Icon) ([]byte, error) {
	return icons.ReadFile("ico/" + string(name))
}
