package api

import (
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/api/router"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/cfg"
)

func initApp(srv *Server) {
	authRouter := router.NewAuthRouter()
	vaultRouter := router.NewVaultRouter()

	api := srv.Group("/api")
	{
		auth := api.Group("/auth")
		{
			auth.POST("/register", authRouter.RegisterUser)
			auth.POST("/activate", authRouter.ActivateUser)
			auth.POST("/login", authRouter.LoginUser)
		}
		vault := api.Group("/vault")
		{
			vault.POST("/create", vaultRouter.Create)
			vault.POST("/read", vaultRouter.Read)
			vault.POST("/update", vaultRouter.Update)
			vault.POST("/delete", vaultRouter.Delete)
		}
	}
}

func StartService() {
	srv := New(cfg.NewParsedConfig())
	initApp(srv)
	srv.StartServerWithGracefulShutdown()
}

func StartServiceChan() chan struct{} {
	serviceChan := make(chan struct{})
	go func() {
		StartService()
		<-serviceChan
	}()
	return serviceChan
}
