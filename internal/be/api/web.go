package api

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/cfg"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/tools"
	"golang.org/x/net/context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Server struct to do app server startup.
type Server struct {
	*http.Server
	*gin.Engine
	Cfg *cfg.Config
}

// New is a function that creates a new application server.
// Accepts *Config server configurations as arguments.
func New(config ...*cfg.Config) *Server {
	var conf *cfg.Config
	if len(config) > 0 {
		conf = config[0]
	}
	if conf == nil {
		panic("aaaaaahh! invalid or null configuration!")
	}

	handler := gin.Default()
	if conf.Env == tools.Production {
		gin.SetMode(gin.ReleaseMode)
	}
	srv := &http.Server{
		Addr:    conf.Host + ":" + conf.Port,
		Handler: handler,
	}
	return &Server{Server: srv, Engine: handler, Cfg: conf}
}

// StartServerWithGracefulShutdown function for starting server with a graceful shutdown.
func (srv *Server) StartServerWithGracefulShutdown() {
	go func() {
		srv.StartServer()
	}()

	// Wait for interrupt signal to gracefully shutdown the server
	sigint := make(chan os.Signal)
	signal.Notify(sigint, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	<-sigint
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic":  "shutdown failure",
			"reason": fmt.Sprintf("%v", err),
		}).Error("Unable to shutdown server.")
	}
	logrus.Info("Server is shutting down.")
}

// StartServer func for starting a simple server.
func (srv *Server) StartServer() {
	// Run server.
	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		logrus.WithFields(logrus.Fields{
			"topic":  "server status",
			"status": "not running",
			"reason": fmt.Sprintf("%v", err),
		}).Fatal("Server is not running.")
	}
}
