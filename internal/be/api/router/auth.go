package router

import (
	"github.com/gin-gonic/gin"
)

type AuthRouter interface {
	RegisterUser(ctx *gin.Context)
	ActivateUser(ctx *gin.Context)
	LoginUser(ctx *gin.Context)
}

type authRouter struct{}

func NewAuthRouter() AuthRouter {
	return &authRouter{}
}

func (r *authRouter) RegisterUser(ctx *gin.Context) {
}

func (r *authRouter) ActivateUser(ctx *gin.Context) {
}

func (r *authRouter) LoginUser(ctx *gin.Context) {
}
