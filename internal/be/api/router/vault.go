package router

import (
	"github.com/gin-gonic/gin"
)

type VaultRouter interface {
	Create(ctx *gin.Context)
	Read(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type vaultRouter struct{}

func NewVaultRouter() VaultRouter {
	return &vaultRouter{}
}

func (r *vaultRouter) Create(ctx *gin.Context) {
}

func (r *vaultRouter) Read(ctx *gin.Context) {
}

func (r *vaultRouter) Update(ctx *gin.Context) {
}

func (r *vaultRouter) Delete(ctx *gin.Context) {
}
