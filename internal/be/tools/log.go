package tools

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm/logger"
	"time"
)

type GormLogger struct {
	*logrus.Logger
}

func (l *GormLogger) LogMode(level logger.LogLevel) logger.Interface {
	return l
}

func (l *GormLogger) Info(ctx context.Context, s string, i ...interface{}) {
	logrus.Infof(s, i)
}

func (l *GormLogger) Warn(ctx context.Context, s string, i ...interface{}) {
	logrus.Warnf(s, i)
}

func (l *GormLogger) Error(ctx context.Context, s string, i ...interface{}) {
	logrus.Errorf(s, i)
}

func (l *GormLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	logrus.Trace(fmt.Sprintf("%v", err))
}

func ParseLogLevel(lvl string) logrus.Level {
	parsed, err := logrus.ParseLevel(lvl)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"topic":   "logging configuration",
			"event":   "using default",
			"level":   lvl,
			"default": "info",
		}).Warn("Failed parsing Logger level.")
		return logrus.InfoLevel
	}
	return parsed
}
