package tools

import (
	"github.com/sirupsen/logrus"
	"os"
	"strings"
)

type Appenv uint32

const (
	Development = iota
	Local
	Staging
	Testing
	Production
)

func Getenv(key string, dft string) string {
	val := os.Getenv(key)
	if val == "" {
		logrus.WithFields(logrus.Fields{
			"topic":   "configuration warning",
			"event":   "using fallback",
			"key":     key,
			"default": dft,
		}).Warn("Key not found.")
		return dft
	}
	return val
}

func ParseEnv(env string) Appenv {
	switch strings.ToLower(env) {
	case "devel", "development":
		return Development
	case "local":
		return Local
	case "stage", "staging":
		return Staging
	case "test", "testing":
		return Testing
	case "prod", "production":
		return Production
	default:
		logrus.WithFields(logrus.Fields{
			"topic":   "application environment configuration",
			"event":   "using default",
			"level":   env,
			"default": "local",
		}).Warn("Failed parsing environment cfg.")
		return Local
	}
}
