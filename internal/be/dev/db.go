package dev

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"
	"gorm.io/gorm"
)

type dummyDB struct {
	*gorm.DB
}

type DB interface {
	DefaultInit()
}

func NewDummyDB(db *gorm.DB) DB {
	return &dummyDB{db}
}

func (d *dummyDB) DefaultInit() {
	if err := d.AutoMigrate(&model.Folder{}, &model.Vault{}); err != nil {
		logrus.Error(err)
		return
	}

	// Create some folders
	if err := d.Create(&model.Folder{Name: "Social"}).Error; err != nil {
		logrus.Error(err)
	}
	if err := d.Create(&model.Folder{Name: "Banking"}).Error; err != nil {
		logrus.Error(err)
	}
	if err := d.Create(&model.Folder{Name: "Fun"}).Error; err != nil {
		logrus.Error(err)
	}

	// Create vault entries
	if err := d.Create(&model.Vault{
		Username: "Leopold",
		Email:    "leo@vanguard.com",
		Password: "8hduS7dŁsa",
		Site:     "https://mysite.com/auth"}).Error; err != nil {
		logrus.Error(err)
	}

	if err := d.Create(&model.Vault{
		Username: "IamHere",
		Email:    "here@localguy.com",
		Password: "lsaopDSAŁwo3",
		Site:     "https://fakebook.com/reg",
		ParentID: 1}).Error; err != nil {
		logrus.Error(err)
	}

	var vaults []model.Vault
	d.Find(&vaults)
	return
}
