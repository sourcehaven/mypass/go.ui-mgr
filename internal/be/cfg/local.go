package cfg

import (
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/tools"
	"strings"
)

type Config struct {
	Host            string       // specifies host that the application will run on
	Port            string       // specifies which port the app should run on
	Env             tools.Appenv // current environment eg.: devel, prod, etc...
	LogLevel        logrus.Level // logging level (trace, debug, info, warn, error)
	DbConnectionUri string       // specifies the db connection string
}

func NewParsedConfig() *Config {
	env := tools.Getenv("MYPASS_ENV", "development")

	env = strings.ToLower(env)
	if env != "" {
		_ = godotenv.Load(".env." + env)
	}
	_ = godotenv.Load()

	const host = "0.0.0.0"
	const port = "7278"
	const level = "info"
	const dburi = "file::memory:?cache=shared"

	return &Config{
		Env:             tools.ParseEnv(env),
		Host:            tools.Getenv("MYPASS_HOST", host),
		Port:            tools.Getenv("MYPASS_PORT", port),
		LogLevel:        tools.ParseLogLevel(tools.Getenv("MYPASS_LOGLEVEL", level)),
		DbConnectionUri: tools.Getenv("MYPASS_DB_CONNECTION_URI", dburi),
	}
}
