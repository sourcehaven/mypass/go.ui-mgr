package db

import "gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"

func (r *Repository) Migrate() error {
	err := r.Db.AutoMigrate(&model.Folder{})
	if err != nil {
		return err
	}

	err = r.Db.AutoMigrate(&model.Vault{})
	if err != nil {
		return err
	}

	err = r.Db.AutoMigrate(&model.Note{})
	if err != nil {
		return err
	}
	return nil
}
