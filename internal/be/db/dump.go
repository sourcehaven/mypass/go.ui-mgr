package db

import (
	"errors"
	"gorm.io/gorm"
)

type dumpTool struct {
	*gorm.DB
}

type DumpTool interface {
	DumpJson() (err error)
	LoadJson(schema []byte) (err error)
}

func NewDumpTool(db *gorm.DB) DumpTool {
	return &dumpTool{db}
}

func (d *dumpTool) DumpJson() (err error) {
	return errors.New("not implemented")
}

func (d *dumpTool) LoadJson(schema []byte) (err error) {
	return errors.New("not implemented")
}
