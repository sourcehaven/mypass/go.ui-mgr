package db

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Connect(conn string, c ...*gorm.Config) (db *gorm.DB) {
	conf := &gorm.Config{}
	if len(c) > 0 {
		conf = c[0]
	}
	db, err := gorm.Open(sqlite.Open(conn), conf)
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	return
}
