package db

import (
	"errors"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/model"
	"gorm.io/gorm"
	"strings"
)

type Repository struct {
	Db *gorm.DB
}

func (r *Repository) GetAllFolder() ([]*model.Folder, error) {
	var folders []*model.Folder
	if err := r.Db.Find(&folders).Error; err != nil {
		return nil, err
	}
	return folders, nil
}

func (r *Repository) GetAllEntry() ([]model.Entry, error) {
	vaults, err := r.GetAllVault()

	if err != nil {
		return nil, err
	}

	notes, err := r.GetAllNote()

	if err != nil {
		return nil, err
	}

	var entries []model.Entry
	for _, v := range vaults {
		entries = append(entries, v)
	}

	for _, v := range notes {
		entries = append(entries, v)
	}

	return entries, nil
}

func (r *Repository) DeleteVault(entry *model.Vault) error {
	if err := r.Db.Delete(entry, entry.GetId()).Error; err != nil {
		return err
	}
	return nil
}

func (r *Repository) DeleteNote(entry *model.Note) error {
	if err := r.Db.Delete(entry, entry.GetId()).Error; err != nil {
		return err
	}
	return nil
}

func (r *Repository) GetVaultById(id uint) (*model.Vault, error) {
	var vault model.Vault
	if err := r.Db.Preload("Folder").First(&vault, id).Error; err != nil {
		return nil, err
	}
	return &vault, nil
}

func (r *Repository) GetAllVault() ([]*model.Vault, error) {
	var vaults []*model.Vault
	if err := r.Db.Preload("Folder").Find(&vaults).Error; err != nil {
		return nil, err
	}

	return vaults, nil
}

func (r *Repository) isFolderFound(name string) (bool, error) {
	if name == "" {
		return false, nil
	}

	var record model.Folder
	if err := r.Db.Where("name = ?", name).First(&record).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (r *Repository) createFolder(tx *gorm.DB, folder *model.Folder) error {
	if err := tx.Create(folder).Error; err != nil {
		if strings.Contains(err.Error(), "UNIQUE constraint failed") {
			return nil
		}
		return err
	}
	return nil
}

func (r *Repository) CreateUpdateVault(entry *model.Vault) error {
	if err := r.Db.Save(entry).Error; err != nil {
		return err
	}

	return nil
}

func (r *Repository) GetNoteById(id uint) (*model.Note, error) {
	var note model.Note
	if err := r.Db.Preload("Folder").First(&note, id).Error; err != nil {
		return nil, err
	}
	return &note, nil
}

func (r *Repository) GetAllNote() ([]*model.Note, error) {
	var notes []*model.Note
	if err := r.Db.Preload("Folder").Find(&notes).Error; err != nil {
		return nil, err
	}

	return notes, nil
}

func (r *Repository) CreateUpdateNote(entry *model.Note) error {
	if err := r.Db.Save(entry).Error; err != nil {
		return err
	}

	return nil
}
