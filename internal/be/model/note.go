package model

import (
	"gorm.io/gorm"
)

type Note struct {
	gorm.Model
	Name      string
	Note      string
	Source    string
	Favourite bool

	FolderID uint
	Folder   *Folder `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

func NewNote(name string, note string, source string, favourite bool, folder *Folder) *Note {
	return &Note{
		Name:      name,
		Note:      note,
		Source:    source,
		Favourite: favourite,
		Folder:    folder,
	}
}

func (e *Note) GetId() uint {
	return e.ID
}

func (e *Note) GetName() string {
	return e.Name
}

func (e *Note) GetFolder() *Folder {
	return e.Folder
}

func (e *Note) GetNote() string {
	return e.Note
}

func (e *Note) GetFavourite() bool {
	return e.Favourite
}

func (e *Note) GetSource() string {
	return e.Source
}

func (e *Note) SetName(name string) {
	e.Name = name
}

func (e *Note) SetFolder(folder *Folder) {
	e.Folder = folder
}

func (e *Note) SetNote(note string) {
	e.Note = note
}

func (e *Note) ToggleFavourite() { e.Favourite = !e.Favourite }

func (e *Note) SetFavourite(fav bool) {
	e.Favourite = fav
}
