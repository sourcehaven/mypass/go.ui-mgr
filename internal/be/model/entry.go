package model

import (
	"reflect"
	"strconv"
)

type Entry interface {
	GetId() uint        // Unique identifier for the entity
	GetName() string    // Name of the entry
	SetName(string)     // Change entry name
	GetFolder() *Folder // Folder the entry belongs to
	SetFolder(*Folder)  // Change Folder
	GetNote() string    // Additional Note text for the entry
	SetNote(string)     // Change Note
	GetFavourite() bool // Favourite or not
	ToggleFavourite()   // Toggle Favourite
	SetFavourite(bool)  // Set Favourite
	GetSource() string  // "MyPass" by default. In case of import, can be "LastPass", "NordPass", etc.
}

func GroupEntriesByFolder(entries []Entry) map[string][]Entry {
	entriesByFolder := make(map[string][]Entry)

	for _, entry := range entries {
		folder := entry.GetFolder()

		folderName := ""
		if folder != nil {
			folderName = folder.GetName()
		}
		entriesByFolder[folderName] = append(entriesByFolder[folderName], entry)
	}

	return entriesByFolder
}

func GetUniqueEntryId(entry Entry) string {
	return strconv.FormatUint(uint64(entry.GetId()), 10) + reflect.TypeOf(entry).String()
}
