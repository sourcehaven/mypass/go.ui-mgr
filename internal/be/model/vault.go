package model

import (
	"gorm.io/gorm"
)

type Vault struct {
	gorm.Model
	Name      string
	Username  string
	Email     string
	Password  string
	Site      string
	Note      string
	Source    string
	Favourite bool
	Url       string

	/* Relationships */

	ParentID int
	Parent   *Vault `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	ChildID  int
	Child    *Vault

	FolderID uint
	Folder   *Folder `gorm:"foreignKey:FolderID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

func NewVault(
	name string,
	username string,
	email string,
	password string,
	site string,
	note string,
	source string,
	favourite bool,
	url string,
	folder *Folder,
) *Vault {
	return &Vault{
		Name:      name,
		Username:  username,
		Email:     email,
		Password:  password,
		Site:      site,
		Note:      note,
		Source:    source,
		Favourite: favourite,
		Url:       url,
		Folder:    folder,
	}
}

func (e *Vault) GetId() uint {
	return e.ID
}

func (e *Vault) GetName() string {
	return e.Name
}

func (e *Vault) SetName(name string) {
	e.Name = name
}

func (e *Vault) GetFolder() *Folder {
	return e.Folder
}

func (e *Vault) SetFolder(f *Folder) { e.Folder = f }

func (e *Vault) GetNote() string {
	return e.Note
}

func (e *Vault) SetNote(note string) { e.Note = note }

func (e *Vault) GetSource() string {
	return e.Source
}

func (e *Vault) GetFavourite() bool {
	return e.Favourite
}

func (e *Vault) ToggleFavourite() {
	e.Favourite = !e.Favourite
}

func (e *Vault) SetFavourite(fav bool) {
	e.Favourite = fav
}

func (e *Vault) GetUrl() string {
	return e.Url
}

func (e *Vault) SetUrl(url string) { e.Url = url }

func (e *Vault) GetUsername() string { return e.Username }

func (e *Vault) SetUsername(username string) { e.Username = username }

func (e *Vault) GetPassword() string { return e.Password }

func (e *Vault) SetPassword(password string) { e.Password = password }
