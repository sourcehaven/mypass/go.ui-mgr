package model

import "gorm.io/gorm"

type Folder struct {
	gorm.Model
	Name string `gorm:"unique"`
}

func NewFolder(name string) *Folder {
	return &Folder{
		Name: name,
	}
}

func (e Folder) GetId() uint { return e.ID }

func (e Folder) GetName() string {
	return e.Name
}
