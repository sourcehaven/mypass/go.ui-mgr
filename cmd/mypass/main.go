package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/api"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/db"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/be/tools"
	"gitlab.com/sourcehaven/mypass/go.ui-mgr/internal/fe/ui"
	"gorm.io/gorm"
	"os"
	"path"
	"strconv"
)

func getUiConfigPath() string {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err.Error())
	}

	return path.Join(homeDir, ".mypass/ui/config.yaml")
}

func setLogLevel(defaultLogLevel logrus.Level) {
	lvl := defaultLogLevel

	// First command line argument is used for setting log level
	if len(os.Args) > 1 {
		logLevel := os.Args[1]
		if num, err := strconv.Atoi(logLevel); err == nil {
			lvl = logrus.Level(num)
		}
	}

	logrus.SetLevel(lvl)
	logrus.Info("Log level set to ", lvl.String())
}

func main() {
	setLogLevel(logrus.DebugLevel)

	apiChan := api.StartServiceChan()
	defer close(apiChan)

	gormDb := db.Connect("test.db", &gorm.Config{Logger: &tools.GormLogger{Logger: logrus.StandardLogger()}})
	repo := &db.Repository{Db: gormDb}

	err := repo.Migrate()
	if err != nil {
		panic(err.Error())
	}

	uiConfig := ui.NewConfig(getUiConfigPath())
	appConfig := &ui.AppConfig{Title: "MyPass", Repo: repo, UiConfig: uiConfig}

	app := ui.NewApp(appConfig)
	app.SetAppIcon()

	app.LogLifecycle()
	app.AddSystemTray()
	app.AddMainMenu()
	app.AddSampleEntries()
	app.AddContent()
	if err = app.ManageConfig(); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic":  "configuration",
			"reason": fmt.Sprintf("%v", err),
		}).Warn("UI failure.")
	}

	app.Window.ShowAndRun()
}
