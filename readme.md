# MyPass Go application management service

This application serves as a service for managing your vaults.
It also features its own UI, however other interfaces could still be used,
as the app exposes certain endpoints that can be used to communicate with the service.

Basically, all other user interfaces need not worry about secret management, as we will
worry about all those stuff here.

## User Interface

This project's user interface is based on a great package, [Fyne](https://fyne.io/).

## Build

### Android

To build an android apk, you can run one of the following commands.

In the first case, you should also need a [FyneApp.toml](cmd/mypass/FyneApp.toml) file.

> ANDROID_NDK_HOME=~/Android/Sdk/ndk/26.1.10909125 fyne package -os android

To override default values in the file:

> ANDROID_NDK_HOME=~/Android/Sdk/ndk/26.1.10909125 fyne package -os android -appID <APPID> -icon <ICON>

## Security

### Safety

Quantum-safe stuff:
https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/quantum-safe-cryptography.html

### Encryption

Encryption should happen on the client-side, preferably by
[XChaCha20](https://www.makeuseof.com/what-is-xchacha20-encryption/), or
[AES-GCM](https://www.cryptosys.net/pki/manpki/pki_aesgcmauthencryption.html).

### Key Derivation

Great tutorial on hashing passwords with
[Argon2](https://www.alexedwards.net/blog/how-to-hash-and-verify-passwords-with-argon2-in-go).
